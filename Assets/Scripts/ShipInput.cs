﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour
{
	public GameObject bullet1;
	//public GameObject BulletPosition;
	public Vector2 localpos;

	public float speed;

    private Vector2 axis;

    public PlayerBehaviour ship;

    // Use this for initialization
    void Start()
    {
		transform.position = localpos;
    }

    // Update is called once per frame
    void Update()
    {
		localpos = transform.position; 
		if (Input.GetKeyDown ("space")) {
			GameObject bullet = (GameObject)Instantiate (bullet1);
			//bullet1.transform.position = BulletPosition.transform.position;
		}

        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        ship.SetAxis(axis);
		Instantiate(bullet1, localpos, Quaternion.identity);
    }
}
